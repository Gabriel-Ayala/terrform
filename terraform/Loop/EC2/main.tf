terraform {
  required_version = "1.1.3"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.72.0"
    }
  }

  backend "s3" {
    bucket  = "tfstate-803209982957"
    key     = "dev/tfstate_ec2/terraform.tfsate"
    region  = "us-east-1"
    profile = "terraform"
  }
}
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}