resource "aws_instance" "web_server" {
  count = lookup(var.instance, local.env)["number"]

  ami           = lookup(var.instance, local.env)["ami"]
  instance_type = lookup(var.instance, local.env)["type"]

  tags = {
    Name = "Web Server ${local.env}"
    Env  = local.env
  }
}