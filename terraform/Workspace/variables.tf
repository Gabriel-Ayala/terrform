variable "aws_region" {
  type = object({
    dev  = string
    prod = string
  })

  description = "(optional) describe your variable"

  default = {
    dev  = "us-east-1"
    prod = "us-east-2"
  }
}
variable "aws_profile" {
  type        = string
  description = "(optional) describe your variable"
  default     = "terraform"
}

variable "instance" {
  type = object({
    dev = object({
      ami    = string
      type   = string
      number = number
    })
    prod = object({
      ami    = string
      type   = string
      number = number
    })
  })

  default = {
    dev = {
      ami    = "ami-04505e74c0741db8d"
      number = 1
      type   = "t2.micro"
    }
    prod = {
      ami    = "ami-0fb653ca2d3203ac1"
      number = 3
      type   = "t2.micro"
    }
  }
}