terraform {
  required_version = "1.1.3"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.72.0"
    }
  }
  backend "s3" {
    bucket         = "tfstate-803209982957"
    key            = "tfstate/terraform.tfsate"
    region         = "us-east-1"
    profile        = "terraform"
    dynamodb_table = "tflock-tfstate-803209982957"
  }
}

provider "aws" {
  region  = lookup(var.aws_region, local.env)
  profile = var.aws_profile
}