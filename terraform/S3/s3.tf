resource "aws_s3_bucket" "teste" {
  bucket = "${var.enviroment}-${random_pet.bucket.id}"
  acl    = "private"

  tags = var.bucket_tag
}