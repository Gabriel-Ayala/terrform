variable "aws_region" {
  type        = string
  description = "aws region settings"
  default     = "us-east-1"
}
variable "aws_profile" {
  type        = string
  description = "aws profile settings"
  default     = "terraform"
}
